const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ['cryptorev.ipfs.infura.io'],
  },
};

module.exports = nextConfig;
